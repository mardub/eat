---
title: "About eat.uppsala.ai"
date: 2023-10-20T19:00
thumb: "Edvard_Munch_Vampire.jpg"
tags:
    - other
---

This blog is built by two passionates of food living in uppsala since a dozen of years. 
Eat.uppsala.ai is the revival of the older site: food.uppsala.ai . This time it is 100% google free, static, and open source: your can download its template in the following gitlab: [gitlab.com/mardub/eat](https://gitlab.com/mardub/eat).

Note that this blog is not sponsored and is made on our free time.

<!-- ![random image](/assets/img/image.jpg) -->

# Contact

eat [@] uppsala.ai 
