---
title: "Mimos"
date: 2023-12-14T14:00
thumb: "Mimos.jpg"
tags:
    - Restaurant
    - Lunch
    - Dinner
---

## Introduction

One of the most interesting food when you are diabetic is libanese: taboule, hummus, aubergine, thin bread... Thanks to our Libanese friend Rima we recently discovered this restaurant: Mimos. 

# [Yocha](https://www.yochastudio.com)
- Fyrislundsgatan 39L, 754 44 Uppsala
- Tel: 018-300 700
- Email: info@mimos.nu
- Site: [http://www.mimos.nu/](http://www.mimos.nu/about.php)

## Perks

- (+) Fresh ingredients
- (+) Very balanced food: salad, hummus dips etc.
- (+) Catering possible
- (+) Open for lunch

## Description

Mimos is a restaurant on the near suburb of Uppsala. You need to know it is there because there is not much reason to hang out in the district. We went to dinner twice in this restaurant. Their food was always freshly made, genuine libanese. I discovered this restaurant by complete chance: one of my friend is Libanese and catered her PhD dinner from this restaurant. I was impressed by the food being fully tasty despite being catered far from any kitchen. And since a lot of libanese famous dishes such as the taboule, is cold based that makes it perfect for parties and serving lot of people.

As for warm dishes we have tried the salmon twice, the quality varies but the first time was amazing, it being cooked perfectly. The second time was decent but not as impressive. I like also their deep fried haloumi and baba ghanoush.

As far as we know it is the best libanese restaurant in Uppsala.


---
## Comments
To make a comment, please send an e-mail to eat (at) uppsala (dot) ai. Your e-mail address is not used for other purposes, and will be deleted after the comment is published. If you don’t want your real name to be published, sign the e-mail with the name you want to appear.
