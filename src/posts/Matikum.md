---
title: "Matikum"
date: 2023-10-27T14:00
thumb: "Matikum.jpg"
tags:
    - Restaurant
    - Lunch
---
# [Matikum](https://www.matikum.se)
- Thunbergsvägen 3, 752 38 Uppsala
- Email: info@matikum.se
- Tel: 018-471 34 00
- Site: [https://www.matikum.se](https://www.matikum.se)

## Perks

- (+) Quality of the ingredients: Fresh most of the time homemade
- (+) One of the best value for money lunch
- (+) Relatively central
(+) The choice between 4 options: meat, fish, vegetarian, vegan
- (+) Sparkling water on tap included
- (-) Maybe hard to find for people outside of university

## Description

Matikum is the university restaurant inside Engelska Parken. Anyone can eat there, no need to be a student or a teacher. This is my favorit place to eat lunch.  let's be honest, when I arrived 11 years ago the food at matikum was not that good. But since a couple of years their quality has completely changed, and now I am looking forward to eat when I go to work! Some might say that their price (115 SEK), but with the inflation it is hard to find cheaper decent option in town.

Here is an example of things they do:
- Once they served a poached egg on top of my meat. The egg was served still warm and yolky in the center, imagine doing that for hundred of people! That is impressive.
- When they serve a pesto, it does not feel canned, the chef seems to be making their sauce themselves.
- They often add a nice toping, like sprinckles of nuts or fresh herbs, that makes me feel like in a real restaurant and not in a cantine


Now to avoid disappointment here is my last trick: it is a university cantine, so you can freely wander around and check peoples plates, take advantage of it look what people have and observe if they finished their dish. With 4 options available there will always be something that looks appealing.

![Matikum](/assets/img/Crusty_Chicken.jpg "Crusty chicken and pasta with crayfish.")

---
## Comments
To make a comment, please send an e-mail to eat (at) uppsala (dot) ai. Your e-mail address is not used for other purposes, and will be deleted after the comment is published. If you don’t want your real name to be published, sign the e-mail with the name you want to appear.
