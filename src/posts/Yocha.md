---
title: "Yocha"
date: 2023-11-27T14:00
thumb: "Yocha_Lunch.jpg"
tags:
    - Restaurant
    - Lunch
---

## Introduction

Are you tired of shoddy princess cakes and other overly sweet Swedish treats? Welcome to Yocha! This tea place serves and sells Asian-fusion fika and accessories. In this article, I will talk about both the lunch option and the fika.

# [Yocha](https://www.yochastudio.com)
- Kungsängsgatan 5B, 75322 Uppsala
- Email: info@yochastudio.se
- Site: [https://www.yochastudio.com](https://www.yochastudio.com/projects-8)

## Perks

- (+) Open every day
- (+) Original Japanese-inspired lunch
- (+) Amazing fresh homemade furion style mochis
- (+) Mooncakes
- (+) Ritual of the teapot
- (+) Personalised own cup on demand
- (+) Central location

### Lunch

Although it is primarily a place to have a fika, Yocha also serves lunch. For 115 SEK, you get a bao (pork or vegetarian), silken tofu, and a salad with edamame beans. The quality is good, the menu is balanced, and it fits the theme of the shop. It is not spicy, and I really appreciate the edamame (steamed immature soy-beans) as sides. The offering adds diversity to the food landscape of Uppsala.

![Yocha](/assets/img/Yocha_Bao.jpg "Picture of the lunch Bao at Yocha.")

![Yocha](/assets/img/Yocha_Sides.jpg "Picture of the lunch sides at Yocha.")

### Tea and Fika

The specialty of Yocha is tea. It is an ideal place to share, as you get a one-liter thermos to brew and share. You will be provided with numerous accessories, and first-timers will be given an explanation on how to use the "justice cup." It is a traditional way of serving tea where one washes the leaves and splits the water in such a way that you get equal-brewed tea in everyone's cup.

![Yocha](/assets/img/Yocha_Tea.jpg "Tea leaves being added to gaiwan.")

If you are not a tea drinker, there are plenty of alternative options, ranging from bubble tea to sesame drinks. But my favorite things by far are the yochi-mochis and the mooncakes. They are freshly made and not too sweet, and whoever you invite is sure not to be disappointed. Unlike Swedish cakes, they are not too sweet and come in a reasonable size.

Finally the large space with its glass window makes it perfect for sitting and reading.

Zoe, the owner, is a passionate entrepreneur who used to work in the film industry in China. I am never tired of listening to her stories! And if you are a regular customer, you can even buy your own cup and keep it there with your name on it. One of my games was to leave a sticky note below the ones of my friends to make them smile :).

---

## Comments
To make a comment, please send an e-mail to eat (at) uppsala (dot) ai. Your e-mail address is not used for other purposes, and will be deleted after the comment is published. If you don’t want your real name to be published, sign the e-mail with the name you want to appear.
