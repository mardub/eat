---
title: "Hassan Grill"
date: 2023-12-14T14:00
thumb: "hassan.jpg"
tags:
    - Restaurant
    - Foodtruck
    - Lunch
    - Dinner
---

## Introduction

Want some streetfood but yet something tasty? Forget the fallafel and try the rulle of Hassan grill!

# [Hassan](https://www.facebook.com/DurumcuHasanUstaUppsala/)
- Vaksalatorg, Vaksalagatan 28
- Tel: 073-383 82 73

## Perks

- (+) Charcoal grilled meat
- (+) Freshly cut parsley and tomatoes
- (+) Accepts cash
- (+) Ayran

## Description

Hassan Grill is a foodtruck located on one of the largest square of Uppsala: Vaksala Torg, in front of UKK. Its speciality is its rulle (some germans may call it "Kebab"). It is a thin bread rolled around some vegetables and pieces of meat. He makes it with freshly cut parsley, tomatoes and a selection of meat grilled in the charcoal. The fish is my favorit and is deep fried. I wish he still had his lever, which he often runs out of. Anyway we have been there for years now, and never disappointed, price and and food remain constant and excellent. We hope to see him again, for long. 

You choose between the red and the white sauce, the roll is still warm in your hand when served. I recommand drinking an Ayran: sort of fermented milk typical of Turkey. 

Last but not least: he seems pretty reliable! Open from Lunch until 21:15 even in winter he saved us a couple of times on our way to the train station or to Bolanderna.



---
## Comments
To make a comment, please send an e-mail to eat (at) uppsala (dot) ai. Your e-mail address is not used for other purposes, and will be deleted after the comment is published. If you don’t want your real name to be published, sign the e-mail with the name you want to appear.
