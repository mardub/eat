module.exports = {
  title: 'Eat@Uppsala',
  description: 'All you can eat @Uppsala',
  keywords: ['restaurant reviews', 'uppsala'],
  url: 'https://eat.uppsala.ai', // your site url without trailing slash
  paginate: 6 // how many posts you want to show for each page
  // uncomment the next line if you want to add disqus to your site
  // disqusShortname: "your-shortname"
};
